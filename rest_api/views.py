import json

from flask import Blueprint, request, jsonify

from controller.led import LedController


def rest_api(controller=LedController):
    api = Blueprint('api', __name__)

    @api.route('/status', methods=['GET'])
    def status():
        map = {
            "power": controller.powerStatus,
            "brightness": controller.brightness,
            "color": controller.color,
            "mode": controller.mode,
            "speed": controller.speed
        }
        return jsonify(map)

    @api.route('/power', methods=['GET', 'POST'])
    def turn():
        if request.method == 'GET':
            return str(controller.powerStatus)
        controller.turn_onoff()
        return '', 200

    @api.route('/color', methods=['GET', 'POST'])
    def color():
        if request.method == 'GET':
            return controller.color

        colors = {'red': "FF0000", 'orange': "FFA500", 'yellow': "FFFF00", 'green': "00FF00", 'light blue': "ADD8E6", 'blue': "0000FF", 'purple': "800080", 'pink': "FFC0CB"}
        data = request.data.decode('utf-8')
        if data in colors.keys():
            controller.set_color(color=colors[data])
            return '', 200
        return 'Wrong input', 422

    @api.route('/brightness', methods=['GET', 'POST'])
    def brightness():
        if request.method == 'GET':
            return controller.brightness.__str__()

        data = request.data.decode('utf-8')
        data = int(data)
        if isinstance(data, int) and 10 <= data <= 100:
            value = round(data, -1)
            controller.set_brightness(brightness=value)
            return '', 200
        return 'Wrong input', 422

    @api.route('/mode', methods=['GET', 'POST'])
    def mode():
        if request.method == 'GET':
            return controller.mode.__str__()

        modes = ["fade", 'in out', 'simple change']
        data = request.data.decode('utf-8')
        if data in modes:
            controller.set_mode(mode=data)
            return '', 200
        return "Wrong input", 422

    @api.route('/speed', methods=['GET', 'POST'])
    def speed():
        if request.method == 'GET':
            return controller.speed.__str__()

        data = int(request.data.decode('utf-8'))
        if isinstance(data, int) and 1 <= data <= 5:
            controller.set_speed(speed=data)
            return '', 200
        return 'Wrong input', 422

    @api.route('/docs', methods=['GET'])
    def docs():
        docs_file = open('docs.json', 'r')
        data = json.load(docs_file)
        docs_file.close()

        return jsonify(data)

    return api
