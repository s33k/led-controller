let power = true;
let color = null;
let mode = null;
let brightness = 100;
let speed = 5;

let xhttp = new XMLHttpRequest();

let functionalButtons = [];
let onOffButton = document.getElementById('power-button');
onOffButton.addEventListener('click', onOff);

let rangeBrightness = document.getElementById('brightness-input');
rangeBrightness.addEventListener('change', () => setBrightness(rangeBrightness.value));
let outputBrightness = document.getElementById('brightness-output');

let rangeSpeed = document.getElementById('speed-input');
rangeSpeed.addEventListener('change', () => setSpeed(rangeSpeed.value));
let outputSpeed = document.getElementById('speed-output');

let colorsButtonsParent = document.getElementById('colors');
for(let i = 0; i<colorsButtonsParent.children.length; i++){
    let colorObj = colorsButtonsParent.children.item(i);
    colorObj.addEventListener('click', () => setColor(colorObj.classList.item(0).toString()));
    functionalButtons.push(colorObj);
}

let modesButtonsParent = document.getElementById('modes');
for(let i = 0; i<modesButtonsParent.children.length; i++){
    let modeObj = modesButtonsParent.children.item(i);
    modeObj.addEventListener('click', () => setMode(modeObj.classList.item(0).toString()));
    functionalButtons.push(modeObj);
}



function pressButton(value = String){

}

function setColor(value = String){
    xhttp.onload = null;
    xhttp.open("POST", 'api/color');
    xhttp.setRequestHeader("Content-type", "text/plain");
    xhttp.send(value.replace("-", " "));
}

function setMode(value = String){
    xhttp.onload = null;
    xhttp.open("POST", 'api/mode');
    xhttp.setRequestHeader("Content-type", "text/plain");
    xhttp.send(value.replace("-", " "));
}

function setPowerButton(value = Boolean) {
    if(value){
        onOffButton.innerHTML = "ON";
        onOffButton.className = 'right success';
    }else{
        onOffButton.innerHTML = "OFF";
        onOffButton.className = 'right error';
    }
}

function onOff(value = Boolean){
    power = !power;
    setPowerButton(power);
    xhttp.onload = null;
    xhttp.open("POST", "api/power");
    xhttp.send();
}

function setBrightness(value = Number){
    outputBrightness.innerText = value.toString();
    xhttp.onload = null;
    xhttp.open("POST", 'api/brightness');
    xhttp.setRequestHeader("Content-type", "text/plain");
    xhttp.send(value.toString());
}

function setSpeed(value = Number) {
    outputSpeed.innerText = value.toString();
    xhttp.onload = null;
    xhttp.open("POST", 'api/speed');
    xhttp.setRequestHeader("Content-type", "text/plain");
    xhttp.send(value.toString());
}

function loadStatus(){
    xhttp.open("GET", 'api/status');
    xhttp.onload = function(e){
      if(xhttp.readyState === 4){
          if(xhttp.status === 200){
              let json = JSON.parse(xhttp.responseText);
                power = json.power;
                brightness = json.brightness;
                color = json.color;
                mode = json.mode;
                speed = json.speed;
                setPowerButton(power);
                outputBrightness.innerText = brightness;
                rangeBrightness.value = brightness;
                outputSpeed.innerText = speed;
                rangeSpeed.value = speed;
          }else{
              console.error(xhttp.statusText);
          }
      }
    };
    xhttp.send();
}

document.onload = loadStatus();