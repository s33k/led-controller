from flask import Flask, render_template

from controller.controller import AbstractController
from rest_api.views import rest_api


def create_app(controller=AbstractController):
    app = Flask(__name__)

    @app.route('/', methods=['GET'])
    def index():
        return render_template('index.html')

    rest_api_blueprint = rest_api(controller)
    app.register_blueprint(rest_api_blueprint, url_prefix='/api')

    return app
