from controller.led import LedController
from rest_api.app import create_app

ledController = LedController(pigpio_on=False)

flask_app = create_app(ledController)

if __name__ == '__main__':
    flask_app.run(host='0.0.0.0', debug=True)
