from enum import Enum


class Signal(Enum):
    TURN_ON = 1
    TURN_OFF = 2
    BRIGHTNESS_10 = 3
    MODE_1 = 14
    COLOR_RED = 18
    SPEED_1 = 25


class Radio:

    signals = {
        Signal.TURN_ON: 1,
        Signal.TURN_OFF: 2,
        Signal.BRIGHTNESS_10: 3,
        Signal.MODE_1: 14,
        Signal.COLOR_RED: 18,
    }

    def send_signal(self, signal=Signal):
        pass

