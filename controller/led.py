import threading

import pigpio

from controller.controller import AbstractController
import asyncio

from controller.mode import SimpleChangeMode, ColorInOutMode, FadeMode


class LedController(AbstractController):
    powerStatus = bool
    mode = str
    color = str
    brightness = int
    speed = int
    RED_PIN = 27
    GREEN_PIN = 17
    BLUE_PIN = 22
    PIGPIO_ON = bool
    MODES = {
        'simple change': SimpleChangeMode(),
        'fade': FadeMode(),
        'in out': ColorInOutMode(),
    }

    def __init__(self, pigpio_on=True):
        self.powerStatus = True
        self.mode = None
        self.color = "0000FF"
        self.brightness = 100
        self.speed = 1
        self.PIGPIO_ON = pigpio_on

        if self.PIGPIO_ON:
            self.pi = pigpio.pi()
            self.pi.set_mode(self.GREEN_PIN, pigpio.OUTPUT)
            self.pi.set_mode(self.BLUE_PIN, pigpio.OUTPUT)
            self.pi.set_mode(self.RED_PIN, pigpio.OUTPUT)
            self.set_color("0000FF")

    def set_mode(self, mode):
        if self.powerStatus:
            self.mode = self.MODES[mode]
            self.color = None
            self._stop_mode_loop()
            run = threading.Thread(target=lambda: asyncio.run(self._start_mode_loop()))
            run.start()

    def _set_led_lights_color(self, red=0, green=0, blue=0):
        red = self._get_right_value(red)
        green = self._get_right_value(green)
        blue = self._get_right_value(blue)

        brightness = self.brightness / 200

        red = int(red * brightness)
        green = int(green * brightness)
        blue = int(blue * brightness)

        if -1 > red > 255 or -1 > green > 255 or -1 > blue > 255: raise Exception
        if self.PIGPIO_ON:
            self.pi.set_PWM_dutycycle(self.RED_PIN, red)
            self.pi.set_PWM_dutycycle(self.GREEN_PIN, green)
            self.pi.set_PWM_dutycycle(self.BLUE_PIN, blue)

    def _get_right_value(self, value):
        if value > 255:
            return 255
        if value < 0:
            return 0
        return value

    def set_color(self, color):
        if self.powerStatus:
            self._stop_mode_loop()
            self.color = color
            self.mode = None
            colors = tuple(int(color[i:i + 2], 16) for i in (0, 2, 4))
            if self.PIGPIO_ON:
                self._set_led_lights_color(red=colors[0], green=colors[1], blue=colors[2])

    def set_brightness(self, brightness=int):
        if self.powerStatus:
            self.brightness = brightness

    def set_speed(self, speed=int):
        if self.powerStatus:
            self.speed = speed

    def turn_onoff(self):
        if self.powerStatus:
            # turn off
            self.powerStatus = False
            self._off()
        else:
            # turn on
            self.powerStatus = True
            self._on()
        return self.powerStatus

    def _on(self):
        if self.color is not None:
            self.set_color(self.color)
        else:
            self.set_mode(self.mode)

    def _off(self):
        if self.PIGPIO_ON:
            self._set_led_lights_color(red=0, green=0, blue=0)
            self._stop_mode_loop()

    def _stop_mode_loop(self):
        try:
            self._mode_loop.cancel()
        except AttributeError:
            pass

    async def _start_mode_loop(self):
        self._mode_loop = asyncio.create_task(self._mode_loop_func())
        await self._mode_loop

    async def _mode_loop_func(self):
        self.mode.restart()
        while True:
            value = self.mode.next()
            print(value)
            self._set_led_lights_color(red=value[0], green=value[1], blue=value[2])
            await asyncio.sleep(1.0 / self.speed)
