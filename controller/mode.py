class AbstractMode:

    index = 0
    colors = [
        (255, 0, 0),
        (255, 165, 0),
        (255, 255, 0),
        (0, 255, 0),
        (173, 216, 230),
        (0, 0, 255),
        (128, 0, 128),
        (255, 192, 203)
    ]
    black = (0, 0, 0)
    generated_color_list = []
    generated_color_list_size = 0

    def __init__(self):
        self._create_colors_list()

    def _create_colors_list(self):
        pass

    def next(self):
        self.index += 1
        if self.index == self.generated_color_list_size:
            self.index = 0

        return self.generated_color_list[self.index]

    def actually(self):
        return self.generated_color_list[self.index]

    def restart(self):
        self.index = 0


class ColorInOutMode(AbstractMode):

    def _create_colors_list(self):
        self.generated_color_list = []
        for color in self.colors:
            step = (int(color[0]/16), int(color[1]/16), int(color[2]/16))
            for i in range(32):
                value = object
                if i < 16:
                    # from black to color
                    value = list(self.black)

                    for z in range(3):
                        value[z] = step[z] * (i+1)
                        if value[z] > 255:
                            value[z] = 255

                else:
                    # from color to black
                    value = list(color)

                    for z in range(3):
                        value[z] = step[z] * (32 - (i+1))
                        if value[z] < 0:
                            value[z] = 0

                value = tuple(value)
                self.generated_color_list.append(value)
        self.generated_color_list_size = len(self.generated_color_list)

    def __str__(self):
        return "Color In Out Mode"


class FadeMode(AbstractMode):

    def _create_colors_list(self):
        self.generated_color_list = []
        for i, color in enumerate(self.colors):
            next_color_index = i + 1
            if next_color_index == len(self.colors):
                next_color_index = 0

            next_color = self.colors[next_color_index]

            step = (-int((color[0] - next_color[0])/16), -int((color[1] - next_color[1])/16), -int((color[2] - next_color[2])/16))
            last_color = color
            for j in range(16):
                value = (last_color[0] + step[0], last_color[1] + step[1], last_color[2] + step[2])
                last_color = value
                self.generated_color_list.append(value)
        self.generated_color_list_size = len(self.generated_color_list)

    def __str__(self):
        return 'Fade Mode'


class SimpleChangeMode(AbstractMode):

    def _create_colors_list(self):
        self.generated_color_list = self.colors
        self.generated_color_list_size = len(self.generated_color_list)

    def __str__(self):
        return 'Simple Change Mode'
