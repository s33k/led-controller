DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

source $DIR/venv_linux/bin/activate
python --version
python3.7 $DIR/run.py